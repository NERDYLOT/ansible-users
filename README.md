Ansible Users
=========

Create a user with required parameters and add their keys.

Requirements
------------

Role Variables
--------------


Dependencies
------------
ansible 2.4

Example Playbook
----------------

    - hosts: all
      roles:
         - ansible-users

License
-------

GPLv3

Author Information
------------------
jlozada2426@protonmail.com

